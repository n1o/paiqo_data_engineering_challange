import pytest
from src.task3 import ConstantExpression, DotExpression, StarExpression, ExpressionCompiler

def test_example():
    assert True

def test_constant_expression():
    ex = ConstantExpression("a")
    assert ex.matches("a")
    assert not ex.matches("b")

def test_dot_expression():
    ex = DotExpression()
    assert ex.matches("a")
    assert ex.matches("z")

    assert not ex.matches(".")

def test_star_expression():
    ex = StarExpression(ConstantExpression("a"))
    assert ex.matches("a")

    with pytest.raises(Exception) as e_info:
        ex = StarExpression(StarExpression(DotExpression()))

def test_compiler():
    compiler = ExpressionCompiler("a.c")

    matcher = compiler.compile()

    assert len(matcher.expressions) == 3

    assert matcher.expressions[0].name == ConstantExpression.name
    assert matcher.expressions[0].expression_value == "a"

    assert matcher.expressions[1].name == DotExpression.name

    assert matcher.expressions[2].name == ConstantExpression.name
    assert matcher.expressions[2].expression_value == "c"


    compiler = ExpressionCompiler("a*")
    matcher = compiler.compile()

    assert len(matcher.expressions) == 1
    assert matcher.expressions[0].name == StarExpression.name
    
    assert matcher.expressions[0].expression.name == ConstantExpression.name
    assert matcher.expressions[0].expression.expression_value == "a"


    with pytest.raises(Exception):
        ExpressionCompiler("a**").compile()

    with pytest.raises(Exception):
        ExpressionCompiler("*").compile()


def test_matcher():

    matcher1 = ExpressionCompiler("aa*b.").compile()

    assert matcher1.match("abc")

    matcher2 = ExpressionCompiler("aa*b.").compile()

    assert not matcher2.match("aaaab")

    matcher3 = ExpressionCompiler(".*bc").compile()

    assert matcher3.match("abcabc")

    matcher4 = ExpressionCompiler("a*ab").compile()
    assert matcher4.match("aaab")
    assert matcher4.match("ab")