# Overview

Bunch of code for [Paiqo](https://paiqo.com/en/) Data Enginnering Challenge.

# Solutions
1. [Task 1](./docs/task1.md)
2. [Task 2](task2.ipynb)
3. [Task 3](task3.ipynb)
    * there is also an [source file](./src/task3.py)
    * and [unit tests](./test/task_3_tests.py)  
    
# Tools
* [Poetry](https://python-poetry.org/) is used to manage project 
* [Jupyter](https://jupyter.org/) for explanatory notebooks

# Project Strucutre
* src folder contains most of the source code
* tests folder contains unit tests

# Poetry
Try to install it from the link provided above. 

## Install deps
From to project root folder run

```
poetry install
```

## Running Tests
From to project root folder run

```
poetry run pytest test/*
```

## Running jupyter
From the project root folder run

```
poetry run jupyter-lab
```