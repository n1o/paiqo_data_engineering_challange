
Notes:

* the employee talbe contains a boolean variable if he is a technitian or not,  this is technically not necessary since we could distinguish between technicians and factory workers by having an medical exam or not, however there is no hard requirement not allowing technitians to perform medical checkups
* Answer to question 2
    - I would use an pre insert/update trigger to check if an employee that is to perform if an employee is also certified on a model he is to work on, this is easy we can just look up the cars akfz_model number and check if the engineer has the required certification
    - however I am more of a fan having an lean database and prefer doing this kind of checks in application code instead


```mermaid
erDiagram
    CAR }|--|| AKFZ_CAR_MODEL : manufatured
    CAR {
        string registration_number PK
        string licence_plate
        string akfz_model_number "FK AKFZ_CAR_MODEL.model_number"
    }

    AKFZ_CAR_MODEL {
        string model_number PK
        string model_name
        integer co2_emission "stored as kg/l"
        integer introduction_year
    }
    EMPLOYEE {
        string SVN PK
        string name
        string address
        string phone_number
        number salary
        technitian boolean
    }

    CERTIFICATION }|--o| EMPLOYEE: has
    CERTIFICATION }|--o| AKFZ_CAR_MODEL: belongs
    CERTIFICATION {
        string employee_svn PK "FK employee.svn"
        string akfz_model_number PK "FK akfz_car_model.model_number"
    }

    MEDICAL_EXAM ||--o| EMPLOYEE: has
    MEDICAL_EXAM {
        string employee_svn PK "FK EMPLOYEE.SVN"
        DATE last_exam "index"
    }

    UNION_MEMBERSHIP ||--|| EMPLOYEE: belongs


    UNION ||--|{ UNION_MEMBERSHIP: defined
    UNION {
        integer union_id PK
        JSON union_info
    }

     UNION_MEMBERSHIP {
        string employee_svn PK "FK EMPLOYEE.svn"
        integer union_id PK "FK UNION.union_id"
    }

    ANUAL_ROADWORTHNIESS_TEST |{--|| CAR: test_result
    ANUAL_ROADWORTHNIESS_TEST |{--|| CERTIFICATION: performed_test

    ANUAL_ROADWORTHNIESS_TEST {
        string car_registration_number PK
        DATE year PK
        string employee_svn PK "FK certification.employee_svn"
        integer test_number PK "FK roadworthiness_test.test_number"
        integer test_score
    }

    ROADWORTHNIESS_TEST ||--|{ ANUAL_ROADWORTHNIESS_TEST: test

    ROADWORTHNIESS_TEST {
        integer test_number PK
        string test_name 
        integer max_score
    }
    
````

Disclaimer:
* the last time I wrote or saw ER diagrams was like 11 years ago, I hope I did not produce total garbage