from abc import ABC
from typing import List

# Lets Build up a state machine since it is a funny regex example
class Expression(ABC):
    def matches(self, value: str) -> bool:
        raise Exception("NOT IMPLEMENTED")

class ConstantExpression(Expression):
    name = "contant"

    def __init__(self, expression_value: str) -> None:
        self.expression_value = expression_value

    def matches(self, value: str) -> bool:
        return self.expression_value == value

class DotExpression(Expression):
    name = "dot"

    def matches(self, value: str) -> bool:
        return value.islower()

class StarExpression(Expression):
    name = "star"

    def __init__(self, expression: Expression) -> None:
        if not expression:
            raise Exception("You cannot start with a *")
        if expression.name == self.name:
            raise Exception("Cannot have two * expressions afther each other")
        self.expression = expression

    def matches(self, value: str) -> bool:
        return self.expression.matches(value) # This matches exactly


class Matcher():
    def __init__(self, expressions: List[Expression]) -> None:
        self.expressions = expressions
    
    def match(self, string: str) -> bool:
        """
        Returns true if the input string matches the compiled expression tree
        """
        if not string:
            raise Exception("Nothing to match on")
        
        return self._match(string, self.expressions)

    def _match(self, string: str, expression_chain: List[Expression]) -> bool:
        
        head, tail  = string[:1], string[1:]
        head_expression, tail_expression = expression_chain[:1], expression_chain[1:]

        if head and head_expression:
            head = head[0]
            head_expression = head_expression[0]

            if head_expression.name == StarExpression.name:
                if head_expression.matches(head):
                    # Lets peek forward without the star expression maybe we can terminate
                    _match = self._match(string, tail_expression)
                    if _match:
                        return True # We walked all the way down the expresion tree
                    else:
                        return self._match(tail, tail_expression) # here we match the * at least once
                else:
                    return self._match(string, tail_expression) # There were 0 matches on * expression
            elif head_expression.matches(head):
                return self._match(tail, tail_expression) # Move down the expression tree
            else:
                return self._match(tail, self.expressions) # start the maching again
        
        if not head and head_expression:
            return False # we run out of letters in our input string
        
        if not head_expression:
            return True # we walked all the way down to the end of the expression tree


# Hacky lexer tokenizer :D
class ExpressionCompiler():

    def __init__(self, expression: str) -> None:
        """
        expression: a simple regex expression
        """
        if not expression:
            raise Exception("Cannot compile empty string")

        self.expression  = expression
        self._index = 0

    # Meh implementation since we cannot multiple peeks 
    def _peek(self) -> str:
        return self.expression[self._index] if len(self.expression) > self._index else None
    
    def _next_token(self) -> str:
        token =  self.expression[self._index] if len(self.expression) > self._index else None
        self._index += 1
        return token

    def compile(self) -> Matcher:
        """
        Returns an expression tree matcher
        """
        expression_chain: List[Expression] = []

        while (token := self._next_token()):
            peek = self._peek()

            expression: Expression = None 

            if token == '*':
                raise Exception("No preceding value before *")

            if token.isalpha() and token.islower():
                expression = ConstantExpression(token)
            if token == ".":
                expression = DotExpression()
            
            if peek == "*":
                expression = StarExpression(expression)
                token = self._next_token()

            expression_chain.append(expression)
        
        return Matcher(expression_chain)